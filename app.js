/**
 * Created by michael.essandoh on 15/12/2016.
 */
// call the packages we need
var express = require('express'),
    expressValidator = require('express-validator'),
    bodyParser = require('body-parser'),
    compression = require('compression'),
    pdf = require('html-pdf'),
    app = express();

app.use(bodyParser.urlencoded({ extended: true, limit: '100mb' }));
app.use(bodyParser.json({ limit: '100mb' }));
app.use(expressValidator());
app.use(compression());


if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status( err.code || 500 )
        .json({
          status: 'error',
          message: err
        });
  });
}
else
{
  app.use(function(err, req, res, next) {
    res.status(err.status || 500)
        .json({
          status: 'error',
          message: err.message
        });
  });
}

app.use(function(err, req, res, next) {
  res.status( err.code || 500 )
      .json({
        status: 'error',
        message: err
      });
});

var port = process.env.PORT || 2000;

var router = express.Router();

var renderHtml = require('./modules/html');

router.get('/', function(req, res) {
  res.json({ message: 'Welcome to the Bearded Renderers API!' });
});

router.post('/render_html', renderHtml.getPdf);

app.use('/api', router);

app.listen(port);
console.log('PDF Renderer started on ' + port);