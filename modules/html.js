/**
 * Created by michael.essandoh on 12/15/2016.
 */
var pdf = require('html-pdf');

config = {
    "height": "1123px",
    "width": "794px",
    "border": {
        "top": "10mm",
        "bottom": "1cm"
    },
    "type": "pdf"
};

exports.getPdf = function(req, res) {
    var schema = {
        'HtmlBody': {
            notEmpty: true,
            errorMessage: 'HtmlBody is required'
        }
    };
    req.check(schema);

    var errors = req.validationErrors();
    if (errors) {
        return res.status(400)
            .json({
                status: 'error',
                message: errors
            });
    }
    else
    {
        var html_body = req.body.HtmlBody;
        var pdf_height = req.body.Height;
        var pdf_width = req.body.Width;
        var pdf_border_top = req.body.BorderTop;
        var pdf_border_bottom = req.body.BorderBottom;

        if (!!pdf_height) {
            config["height"] = pdf_height;
        }
        if (!!pdf_width) {
            config["width"] = pdf_width;
        }
        if (!!pdf_border_top) {
            config["border"].top = pdf_border_top;
        }
        if (!!pdf_border_bottom) {
            config["border"].bottom = pdf_border_bottom;
        }

        pdf.create(html_body, config).toBuffer(function(err, buffer){
            if (err) {
                return res.status(500)
                    .json({
                        status: 'error',
                        message: err
                    });
            }

            return res.json({
                status: 'success',
                data: buffer
            });
        });
    }
};