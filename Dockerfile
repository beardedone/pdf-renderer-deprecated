FROM ubuntu:16.04

MAINTAINER Michael K. Essandoh <mexcon.mike@gmail.com>

# Install Node.js & other dependencies
RUN apt-get update && \
    apt-get -y install curl git wget && \
    curl -sL https://deb.nodesource.com/setup_6.x -o nodesource_setup.sh && \
    bash nodesource_setup.sh && \
    apt-get -y install nodejs build-essential

# Install PhantomJS & dependencies
RUN apt-get -y install chrpath libssl-dev libxft-dev && \
    apt-get -y install libfreetype6 libfreetype6-dev libfontconfig1 libfontconfig1-dev && \
    wget https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2 && \
    tar xvjf phantomjs-2.1.1-linux-x86_64.tar.bz2 -C /usr/local/share/ && \
    ln -sf /usr/local/share/phantomjs-2.1.1-linux-x86_64/bin/phantomjs /usr/local/bin

# Install PM2
RUN npm install -g pm2

# Define working directory
RUN mkdir -p /var/www/pdf-renderer
WORKDIR /var/www/pdf-renderer
ADD . /var/www/pdf-renderer
RUN npm install

EXPOSE 2000

# Copy Calibri fonts, and update font cache
RUN mkdir -p /usr/share/fonts/truetype/calibri
ADD fonts /usr/share/fonts/truetype/calibri/
CMD ["fc-cache", "-f", "-v"]

# Run app
# uses Keymetrics for tracking etc
CMD ["pm2-docker", "--json", "process.yml"] 